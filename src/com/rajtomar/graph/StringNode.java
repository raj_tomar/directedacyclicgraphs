package com.rajtomar.graph;

public class StringNode extends Node<String> {

  /*
   * (non-Javadoc)
   * 
   * @see com.rajtomar.graph.Node#setValue(java.lang.String)
   */
  /**
   * Set the value of the string node.
   * 
   * @param value
   *          Value that you want to store in this node.
   */
  @Override
  public void setValue(String value) {
      this.value = value;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.rajtomar.graph.Node#getValue()
   */
  /**
   * Give the value of the String node.
   * 
   * @return String value of the node
   */  
  @Override
  public String getValue() {
    return this.value;
  }
  
}