package com.rajtomar.graph;

/**
 * Node representation that can hold a Int value.
 * 
 * @extends {@link Node<T>}
 * 
 * @author Raj Tomar
 */
public class IntegerNode extends Node<Integer> {

  /*
   * (non-Javadoc)
   * 
   * @see com.rajtomar.graph.Node#setValue(java.lang.String)
   */
  /**
   * Set the value of the integer node.
   * 
   * @param value
   *          Value that you want to store in this node.
   */
  @Override
  public void setValue(String value) {
    this.value = Integer.valueOf(value);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.rajtomar.graph.Node#getValue()
   */
  /**
   * Give the value of the Integer node.
   * 
   * @return Integer value of the node
   */
  @Override
  public Integer getValue() {
    return this.value;
  }

}