package com.rajtomar.graph;

/**
 * An abstract represents a node inside a graph.
 * 
 * @param <T>
 *          Type of the node
 * 
 * @author Raj Tomar
 */
public abstract class Node<T> {
  T value;

  /**
   * Abstract method to set a value to the node. This method should be implemented
   * by all of it's child classes.
   * 
   * @param value
   *          Input value that you want to set to node
   */
  public abstract void setValue(String value);

  /**
   * Abstract method which gives the value of node. This method should be
   * implemented by all of it's child classes.
   * 
   * @return Value of the node
   */
  public abstract T getValue();
}