package com.rajtomar.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class holds a generic Graph.
 * 
 * @param <E>
 *          Generic type(data type of graph nodes).
 * 
 * @author Raj Tomar
 */
public class Graph<E> {

  private int totalVertices = 0;
  private Map<E, LinkedHashSet<E>> graph = new HashMap<>();

  /**
   * Adds node to graph.
   * 
   * @param node
   *          Generic node
   */
  public void addNode(E node) {
    graph.put(node, null);
    ++totalVertices;
  }

  /**
   * Checks where a Generic node of type {@code E} is exists in the graph of not?
   * 
   * @param node
   *          Generic node
   * @return {@code Boolean true} if not exits in graph, otherwise
   *         {@code Boolean false}
   */
  public boolean hasNode(E node) {
    if (this.graph.containsKey(node)) {
      return true;
    }
    return false;
  }

  /**
   * Creates an Edge between two nodes in the graph.
   * 
   * @param node1
   *          Starting node of edge.
   * @param node2
   *          Ending node of edge.
   * @throws IllegalStateException
   *           If you pass an non existing nodes in a graph as a star/end point of
   *           an edge.
   */
  public void addEdge(E node1, E node2) throws IllegalStateException {
    if (!graph.containsKey(node1) || !graph.containsKey(node2)) {
      throw new IllegalStateException("Invalid graph!!! can not make an edge between: (" + node1 + ", " + node2 + ").");
    }
    LinkedHashSet<E> adjacent = graph.get(node1);
    if (adjacent == null) {
      adjacent = new LinkedHashSet<E>();
      graph.put(node1, adjacent);
    }
    adjacent.add(node2);
  }

  /**
   * Checks whether {@code node1} is connected to {@code node2} or not?
   * 
   * @param node1
   *          Starting node of an Edge.
   * @param node2
   *          End node of an Edge.
   * @return {@code Boolean true} if {@code node1} is connected to {@code node2},
   *         otherwise {@code Boolean false}.
   */
  public boolean isConnected(E node1, E node2) {
    Set<E> adjacent = graph.get(node1);
    if (adjacent == null) {
      return false;
    }
    return adjacent.contains(node2);
  }

  /**
   * Gives all the adjacent nodes of a {@code node}.
   * 
   * @param node
   *          Generic node, whose adjacentNodes you want.
   * @return An all the adjacent nodes of give {@code node}(if any), as an
   *         instance of {@code java.util.collection.LinkedList LinkedList} class.
   *         If adjacent nodes does not exists, it will return an empty instance
   *         {@code java.util.collection.LinkedList LinkedList}
   */
  public LinkedList<E> adjacentNodes(E node) {
    LinkedHashSet<E> adjacent = graph.get(node);
    if (adjacent == null) {
      return new LinkedList<>();
    }
    return new LinkedList<E>(adjacent);
  }

  /**
   * Gives the count of in-degree's of a {@code node}.
   * 
   * @param node
   *          Node, for which, would like to know it's in-degree.
   * @return In-degree of the node.
   */
  public int getInDegreeOfNode(E node) {
    int inDegree = 0;
    for (E startNode : this.graph.keySet()) {
      if (isConnected(startNode, node)) {
        ++inDegree;
      }
    }
    return inDegree;
  }

  /**
   * Gives us all the root nodes exists in the graph.
   * 
   * @return {@link java.util.Collection.List List} of root nodes in the graph
   */
  public List<E> rootNodes() {
    List<E> rootNodes = new ArrayList<>();
    for (Map.Entry<E,LinkedHashSet<E>> entry : this.graph.entrySet()) {
      int inDegree = getInDegreeOfNode(entry.getKey());
      if (inDegree == 0) {
        rootNodes.add(entry.getKey());
      }
    }
    return rootNodes;
  }

  /**
   * Gives us all the leaf nodes exists in the graph.
   * 
   * @return {@link java.util.Collection.List List} of leaf nodes in the graph
   */
  public List<E> leafNodes() {
    List<E> leafNodes = new ArrayList<>();
    for (Map.Entry<E,LinkedHashSet<E>> entry : this.graph.entrySet()) {
      LinkedHashSet<E> adjacent = entry.getValue();
      if (adjacent == null) {
        leafNodes.add(entry.getKey());
      }
    }
    return leafNodes;
  }

  /**
   * Removes all the nodes from the graph and completely resets it.
   */
  public void resetGraph() {
    graph.keySet().removeAll(graph.keySet());
  }
}
