package com.rajtomar.graph;

import java.util.LinkedList;
import java.util.List;

/**
 * Traverse the {@link Graph graph}.
 * 
 * @param <E>
 *          Generic type of the Node inside a graph.
 * 
 * @author Raj Tomar
 */
public class Traversal<E> {
  
  /**
   * Implements DFS(Depth first traversal) traversal technique from the DSA(Data
   * Structure and Algorithms) parts.
   * 
   * @param graph
   *          Graph that you want to traverse.
   * @param visited
   *          {@link java.util.collection.LinkedList LinkedList} of all the
   *          visited nodes.
   */
  public void depthFirst(Graph<E> graph, LinkedList<E> visited) {
    LinkedList<E> nodes = graph.adjacentNodes(visited.getLast());
    if (nodes.size() == 0) {
      printPath(visited);
      return;
    }
    for (E node : nodes) {
      if (visited.contains(node)) {
        continue;
      }
      visited.addLast(node);
      depthFirst(graph, visited);
      visited.removeLast();
    }
  }

  /**
   * Implements DFS(Depth first traversal) traversal technique from the DSA(Data
   * Structure and Algorithms) parts.
   * 
   * @param graph
   *          Graph that you want to traverse.
   * @param visited
   *          {@link java.util.collection.LinkedList LinkedList} of all the
   *          visited nodes.
   * @param destination
   *          Destination node that you're looking for.
   */
  public void depthFirst(Graph<E> graph, LinkedList<E> visited, final E destination) {
    LinkedList<E> nodes = graph.adjacentNodes(visited.getLast());
    for (E node : nodes) {
      if (visited.contains(node)) {
        continue;
      }
      if (node.equals(destination)) {
        visited.add(node);
        printPath(visited);
        visited.removeLast();
        break;
      }
      visited.addLast(node);
      depthFirst(graph, visited, destination);
      visited.removeLast();
    }
  }

  /**
   * Helper method that prints the path that you could take to reach on the given
   * node.
   * 
   * @param visited
   *          {@link java.util.collection.LinkedList<E> LinkedList} visited
   */
  private void printPath(LinkedList<E> visited) {
    for (int i = 0; i < visited.size(); i++) {
      System.out.print(visited.get(i));
      if (i != visited.size() - 1) {
        System.out.print("  --->  ");
      }
    }
    System.out.println();
  }

  /**
   * This will print the paths between two particular nodes in the graph.
   * 
   * @param graph
   *          Graph that you want to traverse
   * @param startNode
   *          Start node from where traversal should be started in the graph.
   * @param destinationNode
   *          Destination node that you're looking for.
   */
  public void pathBetweenNodes(Graph<E> graph, E startNode, E destinationNode) {
    LinkedList<E> visited = new LinkedList<>();
    if (startNode == destinationNode && graph.hasNode(startNode)) {
      System.out.println(destinationNode);
    } else {
      visited.add(startNode);
      this.depthFirst(graph, visited, destinationNode);
    }
  }

  /**
   * Prints all the possible paths that is exists in the given graph.
   * 
   * @param graph
   *          Graph that you want to traverse
   * @throws NullPointerException
   *           If graph is null
   */
  public void traverseAllPahts(Graph<E> graph) throws NullPointerException {
    System.out.println("All the paths using, Root Nodes and Leaf Nodes");
    List<E> roots = graph.rootNodes();
    List<E> leafs = graph.leafNodes();
    for (int i = 0; i < roots.size(); i++) {
      for (int j = 0; j < leafs.size(); j++) {
        this.pathBetweenNodes(graph, roots.get(i), leafs.get(j));
      }
    }
  }
  
  /**
   * Prints all the possible paths that is exists in the given graph.
   * 
   * @param graph
   *          Graph that you want to traverse
   * @throws NullPointerException
   *           If graph is null
   */
  public void traverseAllPahtsFromRootsNodeOnly(Graph<E> graph) throws NullPointerException {
    System.out.println("All the paths using, only Root Nodes.");
    List<E> roots = graph.rootNodes();
    for (int i = 0; i < roots.size(); i++) {
      this.pathStartingFromNode(graph, roots.get(i));
    }
  }
  
  /**
   * This will print all the possible paths initiating from a startNode specified by you in the graph.
   * 
   * @param graph
   *          Graph that you want to traverse
   * @param startNode
   *          Start node from where traversal should be started in the graph
   */
  public void pathStartingFromNode(Graph<E> graph, E startNode) {
    LinkedList<E> visited = new LinkedList<>();
      visited.add(startNode);
      this.depthFirst(graph, visited);
  }

}
