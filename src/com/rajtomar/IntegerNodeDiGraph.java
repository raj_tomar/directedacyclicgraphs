package com.rajtomar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.rajtomar.graph.Graph;
import com.rajtomar.graph.IntegerNode;
import com.rajtomar.graph.Node;
import com.rajtomar.graph.Traversal;

/**
 * This class is a stand alone application class for a graph that contains
 * Integer nodes.
 * 
 * @param <E>
 *          Generic type of the Class, in this example I'm passing Integer here.
 * 
 * @author Raj Tomar
 */
public class IntegerNodeDiGraph<E> {

  public static int totalNodes = 0;
  private static Scanner sc;
  private Graph<Integer> graph;

  /**
   * A helper method that will ask the path of the GRAPH file and read it to
   * initialize total number of node inside a graph.
   */
  private void readFile() {
    Boolean fileFound = false;
    do {
      try {
        System.out.println("Enter the name of the file: ");
        sc = new Scanner(System.in);
        String file = sc.nextLine();
        sc = new Scanner(new File(file));
        totalNodes = sc.nextInt();
        fileFound = true;
      } catch (FileNotFoundException e) {
        System.out.print(
            "We could not be found any file at the location provided by you. If you wanna retry, Please press(y/Y)? ");
        sc = new Scanner(System.in);
        String ch = sc.nextLine();
        if (!ch.equals("y") && !ch.equals("Y")) {
          System.out.println("Application closed");
          System.exit(0);
        }
      }
    } while (!fileFound);
  }
  
  /**
   * This helper method will fill actual edges in a directed graph.
   */
  private boolean fillGraph() {
    while (sc.hasNextLine()) {
      String line = sc.nextLine();
      String[] pieces = line.split(", ");
      if (pieces.length == 2) {
        try {
          Node<Integer> node1 = new IntegerNode();
          node1.setValue(pieces[0]);
          
          Node<Integer> node2 = new IntegerNode();
          node2.setValue(pieces[1]);
          
          this.graph.addEdge(node1.getValue(), node2.getValue());
        } catch(IllegalStateException e) {
          System.out.println(e.getMessage());
          this.graph.resetGraph();
          return false;
        }
      }
    }
    sc.close();
    return true;
  }
  
  /**
   * This method is will initialize the total number of node in a graph by calling
   * {@code #readFile()} method and then it will initialize the node with it's
   * value, followed by the inter linking of nodes by calling {@code #fillGraph()}
   * method. After graph is generated completely, it'll call
   * {@code com.rajtomar.graph.Traversal<E>#traverseAllPahts(this.graph)} which
   * will then print all the path that it has from all of it's root nodes to all
   * of it's leaf nodes.
   */
  public void start() {
    System.out.println("Integer Example");
    String ch;
    do {
      this.readFile();
      this.graph = new Graph<Integer>();
      Node<Integer> node = new IntegerNode();
      for (int i = 0; i < totalNodes; i++) {
        node.setValue(String.valueOf(i));
        this.graph.addNode(node.getValue());
      }
      if (this.fillGraph()) {
        Traversal<Integer> traversal = new Traversal<>();
        traversal.traverseAllPahts(this.graph);
        
        System.out.println();
        traversal.traverseAllPahtsFromRootsNodeOnly(this.graph);
      }
      
      System.out.println("Do you wanna try more? (y/Y)");
      sc = new Scanner(System.in);
      ch = sc.nextLine();
    } while (ch.equals("y") || ch.equals("Y"));
  }

  /**
   * Main method to start a JAVA application. This method will only called if you
   * run the application through {@code DiGraphApp} class.
   * 
   * @param args
   *          Holds all the command line arguments of a JAVA app(if any).
   */
  public static void main(String[] args) {
    IntegerNodeDiGraph<Integer> dag = new IntegerNodeDiGraph<>();
    dag.start();
    System.out.println("Application closed...");
  }
  
}
