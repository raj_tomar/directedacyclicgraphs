/**
 * 
 */
package com.rajtomar;

import java.util.Scanner;

/**
 * This class is just a wrapper that will start our application. after
 * application started, you are able to choose options for creating `node` types in graph(either
 * Integer Node or String Node).
 * 
 * @author Raj Tomar
 */
public class DiGraphApp {

  /**
   * Main method to start a JAVA application, here it will ask inputs from the
   * user and initialize graph accordingly.
   * 
   * @param args Holds all the command line arguments of a JAVA app(if any).
   */
  public static void main(String[] args) {
    System.out.println("Graph Example");
    Integer choice;
    do {
      System.out.println("1. For 'Interger' nodes in graph, please Press 1.");
      System.out.println("2. For 'String' nodes in graph, please Press 2.");
      System.out.println("3. Wanna, Exit? Please Press 0.");
      System.out.println("Please choose one option, from above list...");
      Scanner sc = new Scanner(System.in);
      choice = sc.nextInt();

      switch (choice) {
      case 1:
        IntegerNodeDiGraph<Integer> dagInt = new IntegerNodeDiGraph<>();
        dagInt.start();
        break;

      case 2:
        StringNodeDiGraph<String> dagString = new StringNodeDiGraph<>();
        dagString.start();
        break;

      case 3:
        sc.close();
        System.out.println("Application closed...");
        System.exit(0);
        break;
        
      default:
        System.err.println("WRONG inpu, please try again...");
      }
    } while (true);
  }

}
